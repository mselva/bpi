**CM**

- [CM2](2-iterations.pdf)
- [TA vs SDD](adt_sdd.pdf)


**TDs**

- [TD4. Tableau, tableaux dynamiques et boucles](travaux-diriges/04-boucles-for-et-while/README.md)
- [TD5. Séquences de caractères](travaux-diriges/05-sequences-caracteres/README.md)
- [TD6. Algorithmes de tri](travaux-diriges/06-algorithmes-de-tri/README.md)
- [TD7. Structure de données](travaux-diriges/07-sdd-et-complexite/README.md)
- [TD8. Pierre Larousse](travaux-diriges/08-larousse-et-son-dico/README.md)


**TPs**

- TP5. Convertisseur
    - Exercices
        - [Boucles for](travaux-pratiques/05-convertisseur/exercices/01-boucles-for/README.md)
    - Mini projets
        - [Convertisseur](travaux-pratiques/05-convertisseur/mini-projets/01-convertisseur/README.md)
- TP6. Images PGM
    - Exercices
        - [Le hasard fait bien les choses](travaux-pratiques/06-images-pgm/exercices/01-le-hasard-fait-bien-les-choses/README.md)
    - Mini projets
        - [Images PGM](travaux-pratiques/06-images-pgm/mini-projets/01-images-pgm/README.md)
- TP7. Kaléidoscope
    - Exercices
        - [Ligne de commandes et arguments](travaux-pratiques/07-kaleidoscope/exercices/01-parametres-main/README.md)
    - Mini projets
        - [Kaléidoscope](travaux-pratiques/07-kaleidoscope/mini-projets/01-kaleidoscope/README.md)
- TP8. Serpent
    - Mini projets
        - [Plateau](travaux-pratiques/08-serpent/mini-projets/01-serpent/README.md)
- TP9. Sous-suite
    - Exercices
        - [Tableaux](travaux-pratiques/09-sous-suite/exercices/01-tableaux/README.md)
        - [Lecture de fichier](travaux-pratiques/09-sous-suite/exercices/02-lecture-fichier/README.md)
    - Mini projets
        - [Sous-suite monotone](travaux-pratiques/09-sous-suite/mini-projets/01-sous-suite-monotone/README.md)
- TP10. Tout éteint
    - Exercices
        - [Le juste prix](travaux-pratiques/10-tout-eteint/exercices/01-le-juste-prix/README.md)
    - Mini projets
        - [Tout éteint](travaux-pratiques/10-tout-eteint/mini-projets/01-tout-eteint/README.md)
- TP11. Pivot
    - Exercices
        - [Et ça continue, encore et encore...](travaux-pratiques/11-pivot/exercices/01-et-ca-continue-encore-et-encore/README.md)
    - Mini projets
        - [Pivot](travaux-pratiques/11-pivot/mini-projets/01-pivot/README.md)
- TP12. Examen mi-parcours
- TP13. Mots suivants
    - Exercices
        - [Dictionnaires](travaux-pratiques/13-mots-suivants/exercices/01-dico/README.md)
    - Mini projets
        - [Mots suivants](travaux-pratiques/13-mots-suivants/mini-projets/01-mots-suivants/README.md)
- TPopt2. Échiquier
    - Mini projets
        - [Echiquier](travaux-pratiques/optionnels/01-echiquier/mini-projets/01-echiquier/README.md)
- TPopt3. Blobwars
    - Exercices
        - [Je m'en vais comme un prince !](travaux-pratiques/optionnels/02-blobwars/exercices/01-je-m-en-vais-comme-un-prince/README.md)
    - Mini projets
        - [Blob wars](travaux-pratiques/optionnels/02-blobwars/mini-projets/01-blobwars/README.md)
- TPopt4. Ressources
    - Mini projets
        - [Ressources](travaux-pratiques/optionnels/03-ressources/mini-projets/01-ressources/README.md)
- TPopt5. Performance de SDD
    - Exercices
        - [Le temps qui passe ...](travaux-pratiques/optionnels/04-perf-sdd/exercices/01-mesure-du-temps/README.md)
    - Mini projets
        - [Mesure de perf - sdd](travaux-pratiques/optionnels/04-perf-sdd/mini-projets/01-sdd/README.md)
