**CM**

- [CM1](1-bases.pdf)


**TDs**

- [TD1. Premiers programmes](travaux-diriges/01-c-est-parti/README.md)
- [TD2. Opérateurs booléens](travaux-diriges/02-operateurs-booleens/README.md)
- [TD3. Conditionnelles](travaux-diriges/03-conditionnelles/README.md)


**TPs**

- TP1. Let's go!
    - Exercices
        - [Unix is love](travaux-pratiques/01-premiers-programmes/exercices/01-unixislove/README.md)
        - [Monsieur propre](travaux-pratiques/01-premiers-programmes/exercices/02-monsieur-propre/README.md)
        - [L'âge du capitaine](travaux-pratiques/01-premiers-programmes/exercices/03-age-du-capitaine/README.md)
        - [0+0](travaux-pratiques/01-premiers-programmes/exercices/04-somme/README.md)
        - [Par où on rentre ?](travaux-pratiques/01-premiers-programmes/exercices/05-par-ou-on-rentre/README.md)
        - [Premier module](travaux-pratiques/01-premiers-programmes/exercices/06-modules/README.md)
        - [Tuples](travaux-pratiques/01-premiers-programmes/exercices/07-tuples/README.md)
        - [Structures](travaux-pratiques/01-premiers-programmes/exercices/08-type-structure/README.md)
- TP2. Module SVG
    - Exercices
        - [Création de fichiers](travaux-pratiques/02-module-svg-and-co/exercices/01-ecriture-fichier/README.md)
        - [f-strings](travaux-pratiques/02-module-svg-and-co/exercices/02-fstrings/README.md)
        - [Module SVG](travaux-pratiques/02-module-svg-and-co/exercices/03-svg/README.md)
        - [Installation d'un module](travaux-pratiques/02-module-svg-and-co/exercices/04-installation-d-un-module/README.md)
        - [Interpréteur interactif](travaux-pratiques/02-module-svg-and-co/exercices/05-interpreteur-interactif/README.md)
- TP3. Tortue Logo
    - Mini projets
        - [Tortue Logo](travaux-pratiques/03-tortue-logo/mini-projets/01-tortue-logo/README.md)
- TP4. nirPrfne
    - Mini projets
        - [nirPrfne](travaux-pratiques/04-nirPrfne/mini-projets/01-nirPrfne/README.md)
- TPopt1. Morpion
    - Exercices
        - [Débogage avec pdb](travaux-pratiques/optionnels/01-morpion/exercices/14-debogueur-python/README.md)
    - Mini projets
        - [Morpion](travaux-pratiques/optionnels/01-morpion/mini-projets/01-morpion/README.md)
