# Contenu du cours

Vous trouverez ici tous les supports de cours, de TD et de TP pour
BPI.  Le cours est décomposé en chapitres.  Ces chapitres doivent
être, et seront, traités de façon séquentielle.

## Liste des chapitres

**Cliquez sur le nom du chapitre pour accéder à son contenu : CMs, TDs,
TPs.** Le contenu est aussi accessible via le menu du bandeau du site web ci-dessus.

- **[Bases](1-bases/sommaire.md)** : traite tous les concepts et tous
  les savoir-faire de base permettant d'écrire nos premiers programmes.

- **[Itérations](2-iterations/sommaire.md)** : traite les notions
  fondamentales de boucles `while` et boucles `for` ainsi que les
  tableaux, tableaux dynamiques et dictionnaires.

- **[Références](3-references/sommaire.md)** : traite le concept de
  référence mémoire et la structure de données liste chaînée.

- **[Récursivité](4-recursivite/sommaire.md)** : traite le concept de
  fonctions récursives.

## Organisation d'un chapitre

Dans un chapitre, on trouvera à la fois les supports de CM, de TD et de TP.

Dans le menu TP de chaque chapitre on trouvera des exercices et des
mini-projets.

Chacun des exercices a pour objectif de vous faire travailler un
savoir-faire particulier.  Le travail demandé pour un exercice est
donc souvent assez succinct et le contexte est un peu "artificiel".
Pour vous permettre de vraiment appréhender le savoir-faire associé à
un exercice, une correction détaillée vous est fournie.

Contrairement aux exercices, chacun des mini-projets fait appel à de
nombreux savoir-faire et a pour objectif de vous faire développer un
petit programme réaliste.
