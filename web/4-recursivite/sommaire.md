**CM**

- [CM4](4-recursivite.pdf)


**TDs**

- [TD14. Premières fonctions récursives](travaux-diriges/14-premieres-fonctions-recursives/README.md)
- [TD15. Entiers récursifs](travaux-diriges/15-entiers-recursifs/README.md)
- [TD16. Lancer de dés](travaux-diriges/16-lancer-de-des/README.md)
- [TD17. Le problème de partition](travaux-diriges/17-2-partitions/README.md)


**TPs**

- TP19. Fractales
    - Exercices
        - [Incrémente](travaux-pratiques/19-fractales/exercices/01-incremente/README.md)
        - [Stack overflow](travaux-pratiques/19-fractales/exercices/02-stackoverflow/README.md)
    - Mini projets
        - [Fractales](travaux-pratiques/19-fractales/mini-projets/01-fractales/README.md)
- TP20. Labyrinthe
    - Mini projets
        - [Labyrinthe](travaux-pratiques/20-labyrinthe/mini-projets/01-labyrinthe/README.md)
- TP21. solitaire
    - Mini projets
        - [Solitaire](travaux-pratiques/21-solitaire/mini-projets/01-solitaire/README.md)
