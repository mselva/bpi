**CM**

- [CM3](3-references.pdf)


**TDs**

- [TD9. Classes, instances et références](travaux-diriges/09-classes-objets-references/README.md)
- [TD10. Listes simplement chaînées](travaux-diriges/10-listes-simplement-chainees/README.md)
- [TD11. Exceptions, files et piles](travaux-diriges/11-piles-et-files/README.md)
- [TD12. Redonnons la main](travaux-diriges/12-yieldons/README.md)
- [TD13. Listes doublement chaînées](travaux-diriges/13-listes-doublement-chainees/README.md)


**TPs**

- TP14. Retour examen mi-parcours
- TP15. ListEs
    - Exercices
        - [Première classe](travaux-pratiques/15-listes-sc/exercices/01-premiere-classe/README.md)
        - [Débogage visuel](travaux-pratiques/15-listes-sc/exercices/02-debogage-visuel/README.md)
    - Mini projets
        - [Listes simplement chaînées](travaux-pratiques/15-listes-sc/mini-projets/01-listes-simplement-chainees/README.md)
- TP16. Circulez sentinelle !
    - Mini projets
        - [Listes simplement chaînées triées circulaires avec sentinelle](travaux-pratiques/16-listes-sc-circulaires-senti/mini-projets/01-listes-triees-circulaires/README.md)
- TP17. Yield
    - Exercices
        - [Référence vers une fonction](travaux-pratiques/17-op-listes-sc-yield/exercices/01-reference-vers-fonction/README.md)
        - [yield](travaux-pratiques/17-op-listes-sc-yield/exercices/02-yield/README.md)
    - Mini projets
        - [Opérations sur listes simplement chaînées](travaux-pratiques/17-op-listes-sc-yield/mini-projets/01-operations-sur-listes/README.md)
- TP18. Faut partager
    - Mini projets
        - [Listes simplement chaînées avec partage de suffixes](travaux-pratiques/18-listes-sc-partage-suffixes/mini-projets/01-listes-avec-partage-de-suffixe/README.md)
- TPopt6. Performance du tri
    - Mini projets
        - [Mesure de perf - tri](travaux-pratiques/optionnels/01-analyse-perf-tri/mini-projets/01-perf-sort/README.md)
- TPopt7. Mots suivants yield
    - Mini projets
        - [Mots suivants avec yield](travaux-pratiques/optionnels/02-mots-suivants-yield/mini-projets/01-mots-suivants-yield/README.md)
- TPopt8. Analyse traceur
    - Mini projets
        - [Analyse du module traceur](travaux-pratiques/optionnels/03-analyse-traceur/mini-projets/01-analyse-module-traceur/README.md)
- TPopt9. Analyse de perf listEs chaînées
    - Mini projets
        - [Analyse de performance](travaux-pratiques/optionnels/04-analyse-perf-lc/mini-projets/01-analyse-perfs/README.md)
