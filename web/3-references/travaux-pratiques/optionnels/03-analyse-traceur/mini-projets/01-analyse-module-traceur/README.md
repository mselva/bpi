## Énoncé

 Les deux objectifs, et donc le travail demandé, de ce mini-projet sont les suivants :

 - analyse du code du module du traceur ;
 - proposition et implémentation d'améliorations dans le module.

 Attention, le code du traceur utilise plusieurs concept non abordés dans le cours sur lesquels il faudra faire des recherches pour bien comprendre comment ce module fonctionne.

## Correction
<details markdown="1">
<summary>Cliquez ici pour révéler la correction.</summary>
C'est optionnel, donc pour occuper ceux d'entre vous qui sont très en avance et pourquoi pas récupérer de super bonnes idées :)
</details>
## Exercices

- [Débogage visuel](/3-references/travaux-pratiques/15-listes-sc/exercices/02-debogage-visuel/index.html)
